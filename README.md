# Egendata Identity Provider

## BankID test certificates

1. Create a folder somewhere to place BankID certificates (e.g., `/path/to`)
1. Download [FPTestcert3_20200618.p12](https://www.bankid.com/assets/bankid/rp/FPTestcert3_20200618.p12) to file `/path/to/FPTestcert3_20200618.p12`.
1. Download the "Issuer of server certificate" from https://www.bankid.com/utvecklare/guider/teknisk-integrationsguide/miljoer to file `/path/to/test.ca`.

## Start

```
$ npm start -- --bidPFX=/path/to/FPTestcert3_20200618.p12 --bidCA=/path/to/test.ca --bidPassphrase=qwerty123 --podStorageBaseUrl=http://localhost:3001
```

## Docker

```
$ docker build -t egendata/egendata-identity-provider:latest .
$ docker run -p 3000:3000 -e BID_PFX=//path/to/FPTestcert3_20200618.p12 -e BID_CA=//path/to/test.ca -e BID_PASSPHRASE=qwerty123 -e POD_STORAGE_BASE_URL=http://localhost:3001 egendata/egendata-identity-provider
```
