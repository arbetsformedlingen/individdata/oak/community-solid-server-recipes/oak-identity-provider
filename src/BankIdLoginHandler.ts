import { AccountStore, BadRequestHttpError, BaseInteractionHandler, FoundHttpError, getLoggerFor, InteractionHandlerInput, InternalServerError, KeyValueStorage, Operation, readJsonStream, RegistrationManager } from '@solid/community-server';
import type { InteractionResults } from 'oidc-provider';
import { v4 as uuid } from 'uuid';
import { BankIdClient } from './BankIdClient';
import { strict as assert } from 'assert';
import { BankIdRegistrationParams } from './BankIdRegistrationManager';

const loginView = {
  required: {
    pno: 'string',
  },
} as const;

interface BankIdLoginInput {
  pno: string;
}

/**
 * Handles the submission of the Login Form and logs the user in.
 * Will throw a RedirectHttpError on success.
 */
export class BankIdLoginHandler extends BaseInteractionHandler {
  protected readonly logger = getLoggerFor(this);

  private readonly accountStore: AccountStore;

  private readonly bankIdClient: BankIdClient;

  private readonly registrationManager: RegistrationManager;

  private readonly uuidStorage: KeyValueStorage<string, string>;

  public constructor(accountStore: AccountStore, bankIdClient: BankIdClient, registrationManager: RegistrationManager, uuidStorage: KeyValueStorage<string, string>) {
    super(loginView);
    this.accountStore = accountStore;
    this.bankIdClient = bankIdClient;
    this.registrationManager = registrationManager;
    this.uuidStorage = uuidStorage;
  }

  public async canHandle(input: InteractionHandlerInput): Promise<void> {
    await super.canHandle(input);
    if (input.operation.method === 'POST' && !input.oidcInteraction) {
      throw new BadRequestHttpError(
        'This action can only be performed as part of an OIDC authentication flow.',
        { errorCode: 'E0002' },
      );
    }
  }

  public async handlePost({ operation, oidcInteraction }: InteractionHandlerInput): Promise<never> {
    const { pno } = await this.parseInput(operation);
    assert(
      typeof pno === 'string' && pno.length === 12,
      'Invalid request. The ssn/pno must be format YYYYMMDDXXXX',
    );

    // Initiate and wait for BankID authentication completion
    const { completionData } = await this.bankIdClient.authenticateAndCollect({
      personalNumber: pno,
      endUserIp: "127.0.0.1",
    });

    if (completionData === undefined) {
      throw new InternalServerError('Authentication failed, completion data missing in response');
    }

    const { user } = completionData;
    const { personalNumber, givenName, surname } = user;

    let UUID = await this.uuidStorage.get(personalNumber)
    if (!UUID) {
      UUID = uuid();
      await this.uuidStorage.set(personalNumber, UUID);
    }
    console.log(`UUID = ${UUID}`)

    const validated: BankIdRegistrationParams =  {
      email: `${personalNumber}@example.com`,
      // webId: 'http://localhost:3000/user/profile/card#me',
      password: 'test',
      podName: UUID,
      // template?: string,
      personalNumber,
      createWebId: true,
      register: true,
      createPod: true,
      rootPod: false,
      firstName: givenName,
      lastName: surname,
    };
    const registrationResponse = await this.registrationManager.register(validated, false);
    
    if (registrationResponse.webId === undefined) {
      throw new InternalServerError('Authentication failed, could not get WebID from registration response');
    }

    // Try to log in, will error if email/password combination is invalid
    const webId = await this.accountStore.authenticate(validated.email, validated.password);
    const settings = await this.accountStore.getSettings(webId);
    if (!settings.useIdp) {
      // There is an account but is not used for identification with the IDP
      throw new BadRequestHttpError('This server is not an identity provider for this account.');
    }

    // Update the interaction to get the redirect URL
    const login: InteractionResults['login'] = {
      accountId: webId,
      remember: false,
    };
    oidcInteraction!.result = { login };
    await oidcInteraction!.save(oidcInteraction!.exp - Math.floor(Date.now() / 1000));

    throw new FoundHttpError(oidcInteraction!.returnTo);
  }

  /**
   * Validates the input data. Also makes sure remember is a boolean.
   * Will throw an error in case something is wrong.
   */
  private async parseInput(operation: Operation): Promise<BankIdLoginInput> {
    const { pno } = await readJsonStream(operation.body.data);
    assert(typeof pno === 'string' && pno.length > 0, 'Pno required');
    return { pno };
  }
}
