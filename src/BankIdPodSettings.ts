import { PodSettings } from "@solid/community-server";

/**
 * Metadata related to pod generation.
 * Although the optional fields are not that relevant since this extends Dict,
 * they give an indication of what is sometimes expected.
 */
 export interface BankIdPodSettings extends PodSettings {
  /**
   * Pod storages
   */
  podStorage?: string;
  /**
   * Pno
   */
  pno?: string;
  /**
   * First name of the owner
   */
  firstName?: string;
  /**
   * Last name of the owner
   */
  lastName?: string;
  /**
   * Logotype URI
   */
  logoUri?: string;
}
