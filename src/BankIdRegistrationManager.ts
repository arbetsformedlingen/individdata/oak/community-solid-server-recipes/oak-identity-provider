import { AccountSettings, AccountStore, assertPassword, ClientCredentials, EncodingPathStorage, getLoggerFor, IdentifierGenerator, joinUrl, KeyValueStorage, OwnershipValidator, PodManager, ResourceIdentifier } from '@solid/community-server';
import { BankIdPodSettings } from './BankIdPodSettings';
import { strict as assert } from 'assert';
import { createDpopHeader, generateDpopKeyPair } from '@inrupt/solid-client-authn-core';
import axios from 'axios';
import { buildAuthenticatedFetch } from '@inrupt/solid-client-authn-core';
import { BankIdPodManager } from './BankIdGeneratedPodManager';

export interface BankIdRegistrationManagerArgs {
  /**
   * Used to set the `oidcIssuer` value of newly registered pods.
   */
  baseUrl: string;
  /**
   * Appended to the generated pod identifier to create the corresponding WebID.
   */
  webIdSuffix: string;
  /**
   * Generates identifiers for new pods.
   */
  identifierGenerator: IdentifierGenerator;
  /**
   * Verifies the user is the owner of the WebID they provide.
   */
  ownershipValidator: OwnershipValidator;
  /**
   * Stores all the registered account information.
   */
  accountStore: AccountStore;
  /**
   * Creates the new pods.
   */
  podManager: BankIdPodManager;
  /**
   * Base url of other pod storage server
   */
  podStorageBaseUrl?: string;
  /**
   * Credential storage
   */
   credentialStorage: KeyValueStorage<string, ClientCredentials>,
  /**
   * Client id for this service
   */
  clientId: string,
  /**
   * Client secret for this service
   */
  clientSecret: string,
}

/**
 * The parameters expected for registration.
 */
export interface BankIdRegistrationParams {
  email: string;
  webId?: string;
  password: string;
  personalNumber: string,
  podName?: string;
  template?: string;
  createWebId: boolean;
  register: boolean;
  createPod: boolean;
  rootPod: boolean;
  name?: string;
  firstName?: string;
  lastName?: string;
}

/**
 * The result of a registration action.
 */
export interface BankIdRegistrationResponse {
  email: string;
  webId?: string;
  oidcIssuer?: string;
  podBaseUrl?: string;
  createWebId: boolean;
  register: boolean;
  createPod: boolean;
}

const emailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/u;

/**
 * Supports IDP registration and pod creation based on input parameters.
 *
 * The above behaviour is combined in the two class functions.
 * `validateInput` will make sure all incoming data is correct and makes sense.
 * `register` will call all the correct handlers based on the requirements of the validated parameters.
 */
export class BankIdRegistrationManager {
  protected readonly logger = getLoggerFor(this);

  private readonly baseUrl: string;
  private readonly webIdSuffix: string;
  private readonly identifierGenerator: IdentifierGenerator;
  private readonly ownershipValidator: OwnershipValidator;
  private readonly accountStore: AccountStore;
  private readonly podManager: BankIdPodManager;
  private readonly podStorageBaseUrl: string | undefined;
  private readonly credentialStorage: KeyValueStorage<string, ClientCredentials>;
  private readonly clientId: string;
  private readonly clientSecret: string;

  private myId: string | undefined;
  private mySecret: string | undefined;

  public constructor(args: BankIdRegistrationManagerArgs) {
    this.baseUrl = args.baseUrl;
    this.webIdSuffix = args.webIdSuffix;
    this.identifierGenerator = args.identifierGenerator;
    this.ownershipValidator = args.ownershipValidator;
    this.accountStore = args.accountStore;
    this.podManager = args.podManager;
    this.podStorageBaseUrl = args.podStorageBaseUrl;
    this.credentialStorage = args.credentialStorage;
    this.clientId = args.clientId;
    this.clientSecret = args.clientSecret;

    const idpAccountInput =  {
      email: `000000000000@example.com`,
      // webId: 'http://localhost:3000/user/profile/card#me',
      password: 'test',
      personalNumber: '',
      podName: '000000000000',
      // template?: string,
      createWebId: true,
      register: true,
      createPod: true,
      rootPod: false,
    };

    // Temporary code to add a data consumer identity
    this.register({
      name: 'BNP Paribas Cardif Nordic',
      email: `bnp@example.com`,
      password: 'test',
      personalNumber: '',
      podName: 'bnp',
      createWebId: true,
      register: true,
      createPod: true,
      rootPod: false,
    }).then(async (registrationResponse) => {
      if (!registrationResponse.webId) throw new Error('Failed to register bnp account');

      const settings = await this.accountStore.getSettings(registrationResponse.webId);

      const clientId = 'bnp';
      const clientSecret = 'vXpPxn6YkjeCVszJ';
      settings.clientCredentials = settings.clientCredentials ?? [];
      settings.clientCredentials.push(clientId);
      await this.accountStore.updateSettings(registrationResponse.webId, settings);
      await this.credentialStorage.set(clientId, { secret: clientSecret, webId: registrationResponse.webId });
    });

    // Temporary code to add a data provider identity
    this.register({
      name: 'Arbetsförmedlingen',
      email: `arbetsformedlingen@example.com`,
      password: 'test',
      personalNumber: '',
      podName: 'arbetsformedlingen',
      createWebId: true,
      register: true,
      createPod: true,
      rootPod: false,
    }).then(async (registrationResponse) => {
      if (!registrationResponse.webId) throw new Error('Failed to register arbetsformedlingen account');

      const settings = await this.accountStore.getSettings(registrationResponse.webId);

      const clientId = 'arbetsformedlingen';
      const clientSecret = '2Ghr8gca88LW9zf2';
      settings.clientCredentials = settings.clientCredentials ?? [];
      settings.clientCredentials.push(clientId);
      await this.accountStore.updateSettings(registrationResponse.webId, settings);
      await this.credentialStorage.set(clientId, { secret: clientSecret, webId: registrationResponse.webId });
    });

    // Temporary code to add this identity provider identity
    this.register(idpAccountInput, false).then(async (registrationResponse) => {
      if (!registrationResponse.webId) throw new Error('Failed to register idp account');

      const settings = await this.accountStore.getSettings(registrationResponse.webId)

      //  if (!settings.useIdp) {
      //    throw new BadRequestHttpError('This server is not an identity provider for this account.');
      //  }

      // const name = 'my-token';
      // const id = `${(name as string).replace(/\W/gu, '-')}_${uuidv4()}`;
      // const secret = randomBytes(64).toString('hex');

      // Store the credentials, and point to them from the account
      settings.clientCredentials = settings.clientCredentials ?? [];
      settings.clientCredentials.push(this.clientId);
      await this.accountStore.updateSettings(registrationResponse.webId, settings);
      await this.credentialStorage.set(this.clientId, { secret: this.clientSecret, webId: registrationResponse.webId });

      this.myId = this.clientId;
      this.mySecret = this.clientSecret;
    })
  }

  /**
   * Trims the input if it is a string, returns `undefined` otherwise.
   */
  private trimString(input: unknown): string | undefined {
    if (typeof input === 'string') {
      return input.trim();
    }
  }

  /**
   * Makes sure the input conforms to the following requirements when relevant:
   *  * At least one option needs to be chosen.
   *  * In case a new WebID needs to be created, the other 2 steps will be set to true.
   *  * Valid email/WebID/password/podName when required.
   *  * Only create a root pod when allowed.
   *
   * @param input - Input parameters for the registration procedure.
   * @param allowRoot - If creating a pod in the root container should be allowed.
   *
   * @returns A cleaned up version of the input parameters.
   * Only (trimmed) parameters that are relevant to the registration procedure will be retained.
   */
  public validateInput(input: NodeJS.Dict<unknown>, allowRoot = false): BankIdRegistrationParams {
    const {
      email, password, confirmPassword, webId, podName, register, createPod, createWebId, template, rootPod, personalNumber
    } = input;

    // Parse email
    const trimmedEmail = this.trimString(email);
    assert(trimmedEmail && emailRegex.test(trimmedEmail), 'Please enter a valid e-mail address.');

    assertPassword(password, confirmPassword);

    const validated: BankIdRegistrationParams = {
      email: trimmedEmail,
      password,
      personalNumber: personalNumber as string,
      register: Boolean(register) || Boolean(createWebId),
      createPod: Boolean(createPod) || Boolean(createWebId),
      createWebId: Boolean(createWebId),
      rootPod: Boolean(rootPod),
    };
    assert(validated.register || validated.createPod, 'Please register for a WebID or create a Pod.');
    assert(allowRoot || !validated.rootPod, 'Creating a root pod is not supported.');

    // Parse WebID
    if (!validated.createWebId) {
      const trimmedWebId = this.trimString(webId);
      assert(trimmedWebId && /^https?:\/\/[^/]+/u.test(trimmedWebId), 'Please enter a valid WebID.');
      validated.webId = trimmedWebId;
    }

    // Parse Pod name
    if (validated.createPod && !validated.rootPod) {
      const trimmedPodName = this.trimString(podName);
      assert(trimmedPodName && trimmedPodName.length > 0, 'Please specify a Pod name.');
      validated.podName = trimmedPodName;
    }

    // Parse template if there is one
    if (template) {
      validated.template = this.trimString(template);
    }

    return validated;
  }

  /**
   * Handles the 3 potential steps of the registration process:
   *  1. Generating a new WebID.
   *  2. Registering a WebID with the IDP.
   *  3. Creating a new pod for a given WebID.
   *
   * All of these steps are optional and will be determined based on the input parameters.
   *
   * This includes the following steps:
   *  * Ownership will be verified when the WebID is provided.
   *  * When registering and creating a pod, the base URL will be used as oidcIssuer value.
   */
  public async register(input: BankIdRegistrationParams, allowRoot = false): Promise<BankIdRegistrationResponse> {
    // This is only used when createWebId and/or createPod are true
    let podBaseUrl: ResourceIdentifier | undefined;
    if (input.createPod) {
      if (input.rootPod) {
        podBaseUrl = { path: this.baseUrl };
      } else {
        podBaseUrl = this.identifierGenerator.generate(input.podName!);
        console.log('podBaseUrl = ', podBaseUrl);
      }
    }

    // Create or verify the WebID
    if (input.createWebId) {
      input.webId = joinUrl(podBaseUrl!.path, this.webIdSuffix);
    } else {
      await this.ownershipValidator.handleSafe({ webId: input.webId! });
    }

    // First attempt to authenticate, because if it succeeds,
    // then no account needs to be created
    try {
      await this.accountStore.authenticate(input.email, input.password);
      return {
        webId: input.webId,
        email: input.email,
        oidcIssuer: this.baseUrl,
        podBaseUrl: podBaseUrl?.path,
        createWebId: input.createWebId,
        register: input.register,
        createPod: input.createPod,
      }
    } catch (err) {
      console.log('Failed to authenticate, error:', err);
      // NOP
    }

    // Register the account
    const settings: AccountSettings = {
      useIdp: input.register,
      podBaseUrl: podBaseUrl?.path,
    };

    try {
      await this.accountStore.create(input.email, input.webId!, input.password, settings);
    } catch (err) {
      console.log('Failed to create account, error:', err);
    }

    // Create the pod
    if (input.createPod) {
      const podSettings: BankIdPodSettings = {
        email: input.email,
        webId: input.webId!,
        template: input.template,
        podBaseUrl: podBaseUrl!.path,
        pno: input.personalNumber,
        uuid: input.podName,
        name: input.name,
        firstName: input.firstName,
        lastName: input.lastName,
      };

      // Attach the reference to the other pod storage server
      if (this.podStorageBaseUrl && input.podName) {
        podSettings.podStorage = joinUrl(this.podStorageBaseUrl, input.podName, '/');
      }

      // Set the OIDC issuer to our server when registering with the IDP
      if (input.register) {
        podSettings.oidcIssuer = this.baseUrl;
      }

      try {
        // Only allow overwrite for root pods
        await this.podManager.createPod(podBaseUrl!, podSettings, allowRoot);
      } catch (error: unknown) {
        console.log('Failed to create identity pod, error:', error);
        console.log('Deleting account at this identity-provider...');
        await this.accountStore.deleteAccount(input.email);
        throw error;
      }

      // Let's try to create a pod on the other pod storage server
      try {
        if (this.myId && this.mySecret) {
          console.log(`Fetching access token ...`);
          const dpopKey = await generateDpopKeyPair();
          const authString = `${encodeURIComponent(this.myId)}:${encodeURIComponent(this.mySecret)}`;
          const tokenUrl = `${this.baseUrl}.oidc/token`;
          const response = await axios.post(tokenUrl,
            'grant_type=client_credentials&scope=webid', {
            headers: {
              // The header needs to be in base64 encoding.
              authorization: `Basic ${Buffer.from(authString).toString('base64')}`,
              'content-type': 'application/x-www-form-urlencoded',
              dpop: await createDpopHeader(tokenUrl, 'POST', dpopKey),
            },
          });
          const { access_token: accessToken } = response.data;

          const authFetch = await buildAuthenticatedFetch(fetch, accessToken, { dpopKey });
          console.log(`Trying to create pod: ${this.podStorageBaseUrl}${input.podName}/ ...`);
          await authFetch(`${this.podStorageBaseUrl}${input.podName}/`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'text/turtle',
            },
            body: `
  @prefix dc: <http://purl.org/dc/terms/>.
  @prefix ldp: <http://www.w3.org/ns/ldp#>.
  @prefix posix: <http://www.w3.org/ns/posix/stat#>.
  @prefix xsd: <http://www.w3.org/2001/XMLSchema#>.

  <> a ldp:Container, ldp:BasicContainer, ldp:Resource;
    <http://www.w3.org/ns/auth/acl#accessControl> <.acl>.
            `,
          });

          console.log(`Trying to create pod acl: ${this.podStorageBaseUrl}${input.podName}/.acl ...`);
          await authFetch(`${this.podStorageBaseUrl}${input.podName}/.acl`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'text/turtle',
            },
            body: `
  <${this.podStorageBaseUrl}${input.podName}/.acl#owner> a <http://www.w3.org/ns/auth/acl#Authorization>;
    <http://www.w3.org/ns/auth/acl#agent> <${podSettings.webId}>;
    <http://www.w3.org/ns/auth/acl#mode> <http://www.w3.org/ns/auth/acl#Read>, <http://www.w3.org/ns/auth/acl#Write>, <http://www.w3.org/ns/auth/acl#Control>;
    <http://www.w3.org/ns/auth/acl#accessTo> <${this.podStorageBaseUrl}${input.podName}/>;
    <http://www.w3.org/ns/auth/acl#default> <${this.podStorageBaseUrl}${input.podName}/>.      
            `,
          });
        } else {
          console.log('Skipped creating pod at pod provider, because "this.myId" or "this.mySecret" is not defined');
        }
      } catch (err) {
        console.log('Failed to create user pod in the pod provider, error:', err);
        console.log('Try to delete the identity pod...');
        if (podBaseUrl) {
          console.log('Deleting pod at this identity-provider...');
          await this.podManager.deletePod({ path: `${podBaseUrl.path}` });
          console.log('Deleting account at this identity-provider...');
          await this.accountStore.deleteAccount(input.email);
        }
        throw err;
      }
    }

    try {
      // Verify the account
      // This prevents there being a small timeframe where the account can be used before the pod creation is finished.
      // That timeframe could potentially be used by malicious users.
      await this.accountStore.verify(input.email);
    } catch (err) {
      console.log('Failed to verify email, error:', err);
      throw err;
    }

    return {
      webId: input.webId,
      email: input.email,
      oidcIssuer: this.baseUrl,
      podBaseUrl: podBaseUrl?.path,
      createWebId: input.createWebId,
      register: input.register,
      createPod: input.createPod,
    };
  }
}

