import { addGeneratedResources, cloneRepresentation, ConflictHttpError, getLoggerFor, INTERNAL_QUADS, isContainerIdentifier, PodManager, ResourceIdentifier, ResourcesGenerator, ResourceStore } from '@solid/community-server';
import type { BankIdPodSettings } from './BankIdPodSettings';
import { Store } from 'n3';

export interface BankIdPodManager extends PodManager {
  deletePod: (identifier: ResourceIdentifier) => Promise<void>,
}

/**
 * Pod manager that uses an {@link IdentifierGenerator} and {@link ResourcesGenerator}
 * to create the default resources and identifier for a new pod.
 */
export class BankIdGeneratedPodManager implements PodManager {
  protected readonly logger = getLoggerFor(this);

  private readonly store: ResourceStore;
  private readonly resourcesGenerator: ResourcesGenerator;

  public constructor(store: ResourceStore, resourcesGenerator: ResourcesGenerator) {
    this.store = store;
    this.resourcesGenerator = resourcesGenerator;
  }

  /**
   * Creates a new pod, pre-populating it with the resources created by the data generator.
   * Will throw an error if the given identifier already has a resource.
   */
  public async createPod(identifier: ResourceIdentifier, settings: BankIdPodSettings, overwrite: boolean): Promise<void> {
    this.logger.info(`BankID! Creating pod ${identifier.path}`);
    if (!overwrite && await this.store.hasResource(identifier)) {
      throw new ConflictHttpError(`There already is a resource at ${identifier.path}`);
    }

    const count = await addGeneratedResources(identifier, settings, this.resourcesGenerator, this.store);
    this.logger.info(`Added ${count} resources to ${identifier.path}`);
  }

  public async deletePod(identifier: ResourceIdentifier) {
    const recursiveDeleteResource = async (resource: ResourceIdentifier) => {
      const representation = await cloneRepresentation(await this.store.getRepresentation(resource, { type: { [INTERNAL_QUADS]: 1 } }));
      const quads = representation.metadata.quads();
  
      const n3Store = new Store(quads);
      const contains = n3Store.getQuads(null, 'http://www.w3.org/ns/ldp#contains', null, null);

      for (const containedQuad of contains) {
        const containedResourceIdentifier = { path: containedQuad.object.value };
        if (isContainerIdentifier(containedResourceIdentifier)) {
          // Recursive deletion
          await recursiveDeleteResource(containedResourceIdentifier);
          // Delete container
          await this.store.deleteResource(containedResourceIdentifier);
          console.log('Deleted container:', containedResourceIdentifier);
        } else {
          // Delete resource from store
          await this.store.deleteResource(containedResourceIdentifier);
          console.log('Deleted resource:', containedResourceIdentifier);
        }
      }
    }

    await recursiveDeleteResource(identifier);
  }
}
