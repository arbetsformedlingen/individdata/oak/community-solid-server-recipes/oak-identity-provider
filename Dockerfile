# Build stage
FROM node:lts AS build

## Set current working directory
WORKDIR /build

## Copy the package.json
COPY package*.json ./

## Copy the dockerfile's context's community server files
COPY . .

## cat the test-services-jtech-se.pem file and append it to the trusted certificates file
RUN cat test-services-jtech-se.pem >> /etc/ssl/certs/ca-certificates.crt

## Install packages to node_modules
RUN npm ci --unsafe-perm

# Create image
FROM node:lts-alpine

## Container config & templates for 
# RUN mkdir /config /templates

## Set current directory
WORKDIR /community-server

COPY --from=build /build/package.json .
COPY --from=build /build/config ./config
COPY --from=build /build/templates ./templates
COPY --from=build /build/node_modules ./node_modules
COPY --from=build /build/dist ./dist
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

## Informs Docker that the container listens on the specified network port at runtime
EXPOSE 3000

## Set command run by the container
ENTRYPOINT [ "npx", "community-solid-server" ]

## By default run in memory mode (overriden if passing alternative arguments)
CMD [ \
  "-c", "config/config.json", \
  "-m", ".", \
  "-b", "$BASE_URL", \
  "--bidPFX=$BID_PFX", \
  "--bidCA=$BID_CA", \
  "--bidPassphrase=$BID_PASSPHRASE", \
  "--podStorageBaseUrl=$POD_STORAGE_BASE_URL", \
  "--clientId=$CLIENT_ID", \
  "--clientSecret=$CLIENT_SECRET" \
]
